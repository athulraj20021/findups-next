import React from "react";
import Image from "next/image";

export const FeatureSectionTwo = () => {
  return (
    <section id="features" className="container mx-auto px-10">
      <h2 className="font-bold mb-20 xl:mb-32 text-center text-4xl xl:text-7xl aos-animate">
        Features
      </h2>
      <div className="flex flex-col sm:flex-row mb-20 xl:mb-32 md:max-w-screen-md xl:max-w-full mx-auto">
        <div className="flex-1 mb-6 sm:mb-0 -mx-4 xs:mx-0 items-start">
          <Image
            width={2400}
            height={1598}
            layout="responsive"
            className="w-full sm:max-w-xs xl:max-w-105 2xl:max-w-120 aos-animate image mr-auto"
            src="/assets/images/notification.svg"
            alt=""
          ></Image>
        </div>
        <div className="flex-1 flex flex-col justify-center items-start">
          <div className="max-w-lg xl:max-w-100 xl:ml-5">
            <h5 className="font-bold mb-6 text-2xl xl:text-4xl aos-animate">
              Accurate Notification
            </h5>
            <p className="text-secondary mb-6 text-sm xl:text-lg aos-animate">
              You will get notification about important news based on your
              interest.
            </p>
          </div>
        </div>
      </div>
      <div className="flex flex-col sm:flex-row mb-20 xl:mb-32 md:max-w-screen-md xl:max-w-full mx-auto">
        <div className="flex-1 mb-6 sm:mb-0 -mx-4 xs:mx-0 sm:order-1 order-0">
          <Image
            width={2400}
            height={1598}
            layout="responsive"
            className="w-full sm:max-w-xs xl:max-w-105 2xl:max-w-120 aos-animate"
            src="/assets/images/personal.svg"
            alt=""
            style="transition-duration: 1000ms; transition-delay: 100ms;"
          ></Image>
        </div>
        <div className="flex-1 flex flex-col justify-center items-start sm:order-0 order-0">
          <div className="max-w-lg xl:max-w-100 xl:ml-5">
            <h5 className="font-bold mb-6 text-2xl xl:text-4xl aos-animate">
              Personalized News
            </h5>
            <p className="text-secondary mb-6 text-sm xl:text-lg aos-animate">
              We personalize news for you from hundreds of news publishers.
            </p>
          </div>
        </div>
      </div>
      <div className="flex flex-col sm:flex-row mb-20 xl:mb-32 md:max-w-screen-md xl:max-w-full mx-auto">
        <div className="flex-1 mb-6 sm:mb-0 -mx-4 xs:mx-0">
          <Image
            width={2400}
            height={1598}
            layout="responsive"
            className="w-full sm:max-w-xs xl:max-w-105 2xl:max-w-120 aos-animate"
            src="/assets/images/Search.svg"
            alt=""
            style="transition-duration: 1000ms; transition-delay: 100ms;"
          ></Image>
        </div>
        <div className="flex-1 flex flex-col justify-center items-start">
          <div className="max-w-lg xl:max-w-100 xl:ml-5">
            <h5 className="font-bold mb-6 text-2xl xl:text-4xl aos-animate">
              Advanced Search
            </h5>
            <p className="text-secondary mb-6 text-sm xl:text-lg aos-animate">
              You can experience a better search quality, based on your
              interest.
            </p>
          </div>
        </div>
      </div>
      <div className="flex flex-col sm:flex-row mb-20 xl:mb-32 md:max-w-screen-md xl:max-w-full mx-auto">
        <div className="flex-1 mb-6 sm:mb-0 -mx-4 xs:mx-0 sm:order-1 order-0">
          <Image
            width={2400}
            height={1598}
            layout="responsive"
            className="w-full sm:max-w-xs xl:max-w-105 2xl:max-w-120 aos-animate"
            src="/assets/images/weather.svg"
            alt=""
            style="transition-duration: 1000ms; transition-delay: 100ms;"
          ></Image>
        </div>
        <div className="flex-1 flex flex-col justify-center items-start sm:order-0 order-0">
          <div className="max-w-lg xl:max-w-100 xl:ml-5">
            <h5 className="font-bold mb-6 text-2xl xl:text-4xl aos-animate">
              Daily Weather Update
            </h5>
            <p className="text-secondary mb-6 text-sm xl:text-lg aos-animate">
              Get a daily weather update and weather forecast based on your
              location.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { getTopics } from "../services/news";

export const TopicsSection = () => {
  const [topics, setTopics] = useState([]);
  useEffect(() => {
    async function fetchMyAPI() {
      const res = await getTopics();
      setTopics(res);
    }

    fetchMyAPI();
    return () => {};
  }, []);

  return (
    <section className="relative text-center my-40">
      {/* <div>
        <div className="api-logo a absolute">
          <Image width={80} height={80} className="w-32 xl:w-40" src="/assets/icons/favicon-96x96.png" alt="icon"></Image>
        </div>
      </div> */}
      <div>
        <div className="api-logo b absolute hidden sm:block">
          <Image
            width={60}
            height={60}
            className="w-32 xl:w-40"
            src="/assets/icons/chemistry.png"
            alt="icon"
          ></Image>
        </div>
      </div>
      <div>
        <div className="api-logo c absolute">
          <Image
            width={60}
            height={60}
            className="w-32 xl:w-40"
            src="/assets/icons/healthcare.png"
            alt="icon"
          ></Image>
        </div>
      </div>
      <div>
        <div className="api-logo d absolute hidden sm:block">
          <Image
            width={60}
            height={60}
            className="w-32 xl:w-40"
            src="/assets/icons/economy.png"
            alt="icon"
          ></Image>
        </div>
      </div>
      {/* <div>
        <div className="api-logo e absolute hidden sm:block">
        <Image width={60} height={60} className="w-32 xl:w-40" src="/assets/icons/favicon-96x96.png" alt="icon"></Image>
        </div>
      </div> */}
      <div>
        <div className="api-logo f absolute">
          <Image
            width={60}
            height={60}
            className="w-32 xl:w-40"
            src="/assets/icons/politics.png"
            alt="icon"
          ></Image>
        </div>
      </div>
      <div>
        <div className="api-logo g absolute">
          <Image
            width={60}
            height={60}
            className="w-32 xl:w-40"
            src="/assets/icons/popcorn.png"
            alt="icon"
          ></Image>
        </div>
      </div>
      <div>
        <div className="api-logo h absolute hidden sm:block">
          <Image
            width={60}
            height={60}
            className="w-32 xl:w-40"
            src="/assets/icons/sport-equipment.png"
            alt="icon"
          ></Image>
        </div>
      </div>
      <h3 className="font-bold text-4xl xl:text-7xl mb-10 text-center px-5 aos-animate z-30">
        Findups collects data <span className="hidden xl:block"></span> <br />{" "}
        from all the categories for you
      </h3>
      <p className="text-center text-lg xl:text-2xl w-3/4 sm:w-1/2 mx-auto mb-10 aos-animate z-30">
        Findups collects data from topics like politics, health, sports,
        entertainment, technology, AI, Economy...
        <br />
        You name it we have it.
      </p>
      <div className="w-full grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6 container mx-auto px-2">
        {topics?.map((value) => (
          <Link
            key={value.wordKey}
            href={{
              pathname: "/topics/[topic]",
              query: { topic: value.wordKey },
            }}
            passHref
          >
            <div className="bg-white rounded-lg p-6 cursor-pointer border hover:shadow hover:scale-105 transition ease-in-out">
              <div className="flex items-center space-x-6 py-2 w-full">
                <p className="text-xl text-gray-700 font-normal mb-1 flex justify-between w-full">
                  <span>{value.word} </span>
                  <span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="24px"
                      viewBox="0 0 24 24"
                      width="24px"
                      fill="#000000"
                    >
                      <path d="M0 0h24v24H0z" fill="none" />
                      <path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z" />
                    </svg>
                  </span>
                </p>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </section>
  );
};

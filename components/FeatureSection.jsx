import React from "react";
import Image from "next/image";

export const FeatureSection = () => {
  return (
    <section className="container mx-auto px-10">
      {/* <h2 className="font-bold mb-20 xl:mb-32 text-center text-4xl xl:text-7xl aos-animate">
        Hello
      </h2> */}
      <div className="flex flex-col sm:flex-row mb-20 xl:mb-32 md:max-w-screen-md xl:max-w-full mx-auto">
        <div className="flex-1 mb-6 sm:mb-0 -mx-4 xs:mx-0 items-start">
          <Image
            width={2400}
            height={1598}
            layout="responsive"
            className="w-full sm:max-w-xs xl:max-w-105 2xl:max-w-120 aos-animate image mr-auto"
            src="/assets/images/reading.svg"
            alt=""
          ></Image>
        </div>
        <div className="flex-1 flex flex-col justify-center items-start">
          <div className="max-w-lg xl:max-w-100 xl:ml-5">
            <h5 className="font-bold mb-6 text-2xl xl:text-4xl aos-animate">
              Get A Better Reading Experience..
            </h5>
            <p className="text-secondary mb-6 text-sm xl:text-lg aos-animate">
            We provide a better design and advanced personalization system for you.
            </p>
          </div>
        </div>
      </div>
      <div className="flex flex-col sm:flex-row mb-20 xl:mb-32 md:max-w-screen-md xl:max-w-full mx-auto">
        <div className="flex-1 mb-6 sm:mb-0 -mx-4 xs:mx-0 sm:order-1 order-0">
          <Image
            width={2400}
            height={1598}
            layout="responsive"
            className="w-full sm:max-w-xs xl:max-w-105 2xl:max-w-120 aos-animate"
            src="/assets/images/daily.svg"
            alt=""
            style="transition-duration: 1000ms; transition-delay: 100ms;"
          ></Image>
        </div>
        <div className="flex-1 flex flex-col justify-center items-start sm:order-0 order-0">
          <div className="max-w-lg xl:max-w-100 xl:ml-5">
            <h5 className="font-bold mb-6 text-2xl xl:text-4xl aos-animate">
              Made for Everyday Usage.
            </h5>
            <p className="text-secondary mb-6 text-sm xl:text-lg aos-animate">
            We sort and order every single piece of content for you based on your daily usage.
            </p>
          </div>
        </div>
      </div>
  
    </section>
  );
};

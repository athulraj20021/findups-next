import React from "react";
import Image from "next/image";

export const Banner = () => {
  return (
    <section className="container flex items-center justify-center text-center flex-col pt-16 sm:pb-0 pb-10 xl:pt-40 mx-auto">
      <h1 className=" font-bold mb-12 text-4xl xl:text-7xl ">
        Everything You wants to know <br /> In your daily life.
      </h1>
      <p className="text-lg xl:text-2xl w-10/12 xl:w-1/2 mx-auto">
        Never let you miss anything important for you. Hourly update and
        accurate notification.
      </p>
      <div className="pt-10 relative">
      <a
          target="_blank"
          rel="noreferrer"
          href="https://play.google.com/store/apps/details?id=club.findups.daily&hl=en_IN&gl=US"
        >
          <Image
            src="/assets/images/playstore.png"
            alt="logo"
            width={200}
            height={60}
          ></Image>
        </a>

      </div>

    </section>
  );
};

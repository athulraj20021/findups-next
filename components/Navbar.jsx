import Image from "next/image";
import Link from "next/link";
import React from "react";

export const Navbar = () => {
  const [navbarOpen, setNavbarOpen] = React.useState(false);
  return (

    <nav id="navbar" className="relative flex flex-wrap items-center justify-between py-5 ">
      <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
        <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
          <a
            className=" flex items-center "
          >
            <Image
              src="/assets/icons/logo.png"
              alt="logo"
              width={50}
              height={50}
            ></Image>
            <span className="ml-3 text-3xl font-bold">Findups</span>
          </a>
          <button
            className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
            type="button"
            onClick={() => setNavbarOpen(!navbarOpen)}
          >
            <svg
              className="w-6 h-6"
              fill="#000000"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                clipRule="evenodd"
              ></path>
            </svg>
          </button>
        </div>
        <div
          className={
            "lg:flex flex-grow items-center" +
            (navbarOpen ? " flex" : " hidden")
          }
          id="example-navbar-danger"
        >
          <ul className="flex sm:items-center items-start flex-col lg:flex-row list-none lg:ml-auto">
            <li className="nav-item">
              <a
                className="px-3 py-2 flex items-center text-xs  font-bold leading-snug  hover:opacity-75"
                href="#features"
              >
                <i className=" text-lg leading-lg  opacity-75"></i>
                <span className="text-lg ml-2">Features</span>
              </a>
            </li>
            <li className="nav-item">
              <a
                className="px-3 py-2 flex items-center text-xs  font-bold leading-snug  hover:opacity-75"
                href="#footer"
              >
                <i className=" text-lg leading-lg  opacity-75"></i>
                <span className="text-lg ml-2">Contact Us</span>
              </a>
            </li>
            <li className="nav-item">
              <a
                className="px-3 py-2 flex items-center text-md  font-bold leading-snug  hover:opacity-75"
                href="#download"
              >
                 <button className="bg-red-500 hover:bg-red-400 p-4 text-white font-bold rounded-full">Download</button>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

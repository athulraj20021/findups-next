import React from "react";
import Image from "next/image";

export const Download = () => {
  return (
    <section
      id="download"
      className="container flex items-center justify-center text-center flex-col pt-16 xl:pt-40 mx-auto"
    >
      <h1 className=" font-bold mb-12 text-4xl xl:text-7xl ">
        What are you waiting for?
      </h1>
      <p className="text-lg xl:text-2xl w-10/12 xl:w-1/2 mx-auto">
        That&apos;s right, what are you waiting for? You&apos;ve come this far,
        read all the way to the bottom of the page. The only thing left to do is
        to get yourself over to the Play Store and download Findups today...
      </p>
      <div className="pt-10 relative">
        <a
          target="_blank"
          rel="noreferrer"
          href="https://play.google.com/store/apps/details?id=club.findups.daily&hl=en_IN&gl=US"
        >
          <Image
            src="/assets/images/playstore.png"
            alt="logo"
            width={200}
            height={60}
          ></Image>
        </a>
      </div>
    </section>
  );
};

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'Poppins': ['"Poppins"']
      },
      height: {
        '976': '976px',
      }

    }
  },
  plugins: [ require('@tailwindcss/line-clamp')],
}
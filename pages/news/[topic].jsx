import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import Link from "next/link";
import { getNewsfromQuery, getTopics } from "../../services/news";

export async function getServerSideProps(context) {
  const topic = context.params.topic;
  const data = await getNewsfromQuery(topic, 0);
  const topics = await getTopics();
  return {
    props: { data, page: 0, topic, topics }, // will be passed to the page component as props
  };
}

function NewsDetails({ data, page, topic, topics }) {
  const router = useRouter();
  const [newsList, setNewsList] = useState(data);
  const [pageNo, setPageNo] = useState(0);
  const [selectedTopic, setSelectedTopic] = useState(topic);

  useEffect(() => {
    async function fetchMyAPI() {
      const res = await getNewsfromQuery(selectedTopic, pageNo);
      setNewsList(res);
      window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    }
    fetchMyAPI();
  }, [pageNo, selectedTopic]);

  return (
    <section className=" container mx-auto px-2 py-10">
      <h1 className=" font-bold mb-12 text-4xl xl:text-7xl flex items-center">
        <span className="mr-7">
          <Link href="/">
            <Image
              width={60}
              height={60}
              className="w-32 xl:w-40 cursor-pointer"
              src="/assets/icons/arrow-back.svg"
              alt="icon"
            ></Image>
          </Link>
        </span>{" "}
        {router.query.topic.split("_").join(" ")}
      </h1>
      <div className="flex flex-col md:flex-row gap-10">
        <div className="w-full md:w-5/5">
          <div className="  grid grid-cols-1 sm:grid-cols-2  lg:grid-cols-3 gap-6 ">
            {newsList?.map((news) => (
              <div
                key={news._id}
                className="w-full border rounded-md hover:scale-105 transition ease-in-out"
              >
                <div className="bg-white rounded-lg overflow-hidden mb-10 h-full relative">
                  <img
                    src={news.image}
                    alt="image"
                    className="w-full max-h-48"
                  />
                  <div className="p-8 sm:p-7 md:p-4 xl:p-7 text-center">
                    <h3>
                      <Link href={news.news_url}>
                        <a
                          target="_blank"
                          title={news.title}
                          className="
                             font-semibold
                             text-dark text-xl
                             sm:text-[22px]
                             md:text-lg
                             lg:text-[22px]
                             xl:text-xl
                             2xl:text-[22px]
                             mb-4
                             block
                             hover:text-primary line-clamp-3
                             "
                        >
                          {news.title}
                        </a>
                      </Link>
                    </h3>
                    <p
                      title={news.description}
                      className="text-base text-body-color mb-7  line-clamp-3"
                    >
                      {news.description}
                    </p>
                    <Link href={news.news_url}>
                      <a
                        target="_blank"
                        className="
  
  inline-block
  py-2
  px-7
  border 
  rounded-full
  text-base text-body-color
  font-medium
  hover:border-primary hover:bg-black hover:text-white
  transition ease-in-out
  "
                      >
                        Read More
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="md:w-4/5 flex items-center justify-between mt-10">
            {pageNo != 0 ? (
              <a
                target="_blank"
                onClick={() => setPageNo(pageNo - 1)}
                className="
  
  inline-block
  py-2
  px-7
  border 
  rounded-full
  text-base text-body-color
  font-medium
  cursor-pointer
  bg-black
  text-white
  hover:border-primary hover:bg-primary hover:shadow-md
  hover:scale-105 transition ease-in-out
  "
              >
                Prev
              </a>
            ) : (
              ""
            )}
            {newsList.length != 0 ? (
              <a
                onClick={() => setPageNo(pageNo + 1)}
                className="
  
  inline-block
  py-2
  px-7
  border 
  rounded-full
  text-base text-body-color
  font-medium
  cursor-pointer
  bg-black
  text-white
  ml-auto
  hover:border-primary hover:bg-primary hover:shadow-md
  hover:scale-105 transition ease-in-out
  "
              >
                Next
              </a>
            ) : (
              ""
            )}
          </div>
        </div>

        {/* <div className="w-full md:w-1/5 md
        
        lg:pl-10">
          <div className="sticky top-10">
            <h2 className="font-bold mb-6 text-2xl xl:text-4xl">Topics</h2>
            <ul>
              {topics?.map((topic) => (
                <Link
                  key={topic.wordKey}
                  href={{
                    pathname: "/topics/[topic]",
                    query: { topic: topic.wordKey },
                  }}
                  passHref
                >
                  <li className="py-2 cursor-pointer" key={topic.wordKey}>
                    {topic.word}
                  </li>
                </Link>
              ))}
            </ul>
          </div>
        </div> */}
      </div>
    </section>
  );
}

export default NewsDetails;

import Head from "next/head";
import Image from "next/image";
import { Banner } from "../components/Banner";
import { Download } from "../components/Download";
import { FeatureSection } from "../components/FeatureSection";
import { FeatureSectionTwo } from "../components/FeatureSectionTwo";
import { Footer } from "../components/Footer";
import { Navbar } from "../components/Navbar";
import { TopicsSection } from "../components/TopicsSection";

export default function Home() {
  return (
    <>
      <Head>
        <title>Findups</title>
        <link rel="icon" href="/assets/icons/favicon.ico" />
        <meta
          name="description"
          content="Everything You wants to know In your daily life."
        ></meta>
        <meta name="keywords" content="News,Sports,Technology"></meta>
        <meta name="author" content="Findups"></meta>
        <meta property="og:title" content="Findups" key="title" />
        <meta property="og:image" content="/assets/images/Seo.jpeg" />
        <meta property="og:type" content="application" />
        <meta property="og:description" content="Everything You wants to know In your daily life." />
      </Head>
      <header className="border-b-2">
        <Navbar></Navbar>
      </header>
      <main>
        <Banner></Banner>
        <section className="relative  w-full pt-10 sm:block hidden">
          <Image
            src="/assets/images/MockupWebsite.jpg"
            width={3200}
            height={1598}
            alt="banner"
            layout="responsive"
            objectFit="cover"
            priority
            placeholder="blur"
            blurDataURL="/assets/images/MockupWebsite.jpg"
          ></Image>
        </section>
        <FeatureSection></FeatureSection>
        <TopicsSection></TopicsSection>
        <FeatureSectionTwo></FeatureSectionTwo>
        <Download></Download>
      </main>
      <footer>
        <Footer></Footer>
      </footer>
    </>
  );
}

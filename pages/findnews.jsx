import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import Link from "next/link";
import { getNewsfromId, getTopics } from "../services/news";
import Modal from "react-modal";
import { TopicsSection } from "../components/TopicsSection";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    width: "90%",
    transform: "translate(-50%, -50%)",
  },
};

export async function getServerSideProps(context) {
  const newsId = context.query.newsId;
  const news = await getNewsfromId(newsId);
  const topics = await getTopics();
  return {
    props: { news, topics }, // will be passed to the page component as props
  };
}

function NewsData({ news, topics }) {
  const router = useRouter();
  const [newsData, setNewsData] = useState(news);
  const [isMobile, setIsMobile] = useState(false);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [width, setWidth] = useState();

  function handleWindowSizeChange() {
    setWidth(window.innerWidth);
    if (window.innerWidth < 599) {
      setIsMobile(true);
    } else setIsMobile(false);
  }
  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);
    Modal.setAppElement("body");
    handleWindowSizeChange();
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  function openModal() {
      window.open(newsData.news_url, "_blank");
    
  }

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <section className=" container mx-auto px-0 py-2 sm:px-2 sm:py-10">
      <div className="flex w-full justify-between items-center mb-3 sm:mb-12 px-2">
        <h1 className=" font-medium  text-xl xl:text-5xl flex items-center">
          <span className="mr-3 sm:mr-7">
            <Link href="/">
              <img
                className="w-8 xl:w-20 cursor-pointer"
                src="/assets/icons/arrow-back.svg"
                alt="icon"
              ></img>
            </Link>
          </span>{" "}
          <span className="hidden sm:inline-block">Go to &nbsp;</span>
          Home
        </h1>
        <a
          className="  py-2
            px-2
            rounded-2xl
            text-body-color
            cursor-pointer
            font-medium
            text-black
             text-xl
            transition ease-in-out"
          target="_blank"
          rel="noreferrer"
          href="https://play.google.com/store/apps/details?id=club.findups.daily&hl=en_IN&gl=US"
        >
          Download App
        </a>
      </div>
      <div className="flex flex-col md:flex-row sm:gap-8 items-start">
        <div className="hidden md:inline-block w-1/4">
          <h2 className="font-bold mb-6 text-2xl xl:text-4xl">Topics</h2>
          <ul>
            {topics?.map((topic) => (
              <Link
                key={topic.wordKey}
                href={{
                  pathname: "/topics/[topic]",
                  query: { topic: topic.wordKey },
                }}
                passHref
              >
                <li
                  className={
                    router.query.topic == topic.wordKey
                      ? "font-semibold py-2 cursor-pointer hover:font-semibold"
                      : "py-2 cursor-pointer hover:font-semibold"
                  }
                  key={topic.wordKey}
                >
                  {topic.word}
                </li>
              </Link>
            ))}
          </ul>
        </div>
        <div key={newsData._id} className="w-full md:w-3/4">
          <div className="bg-white sm:rounded-lg sm:border overflow-hidden h-full relative sm:shadow-md">
            <img
              src={newsData.image}
              alt="image"
              className="w-full max-h-96"
              onError={({ currentTarget }) => {
                currentTarget.onerror = null; // prevents looping
                currentTarget.src =
                  "https://wallpaperaccess.com/full/1285952.jpg";
              }}
            />
            <div className="p-4 sm:p-7 md:p-4 xl:p-7 ">
              <h3>
                <Link href={newsData.news_url}>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    title={newsData.title}
                    className="
                             font-medium
                             leading-normal
                             text-dark text-xl
                             text-[24px]
                             sm:text-[24px]
                             md:text-lg
                             lg:text-[24px]
                             xl:text-xl
                             2xl:text-[22px]
                             mb-4
                             block
                             hover:text-primary
                             "
                  >
                    {newsData.title}
                  </a>
                </Link>
              </h3>
              <p
                title={newsData.description}
                className="text-lg text-body-color mb-2 sm:mb-7 text-gray-700"
              >
                {newsData.description}
              </p>

              <button
                onClick={openModal}
                className="
  hidden
  sm:inline-block
  py-2
  px-7
  border 
  rounded-full
  text-base text-body-color
  cursor-pointer
  font-medium
  hover:border-primary hover:bg-black hover:text-white
  transition ease-in-out
  "
              >
                Read More
              </button>
            </div>
          </div>
        </div>
        <div className="w-full md:w-1/4 flex-col flex">
          <div className="  sm:border-2 sm:rounded-md sm:shadow-md flex-col flex items-center justify-center p-5 py-4 sm:py-10 text-center mb-16">
            <Image
              src="/assets/icons/logo.png"
              alt="logo"
              width={100}
              height={100}
            ></Image>
            <h3 className="font-medium text-lg xl:text-lg text-red-400 sm:text-gray-700">
              {" "}
              Download <span className="text-red-600 sm:text-black">FindupsDaily</span> , Daily News App, Designed{" "}
              <span className="text-red-600 sm:text-black">for professionals</span>. <br />
              Absolutely <span className="text-red-600 sm:text-black">Free</span>
            </h3>
            <div className="pt-10 relative">
              <a
                className="  py-3
            px-7
            border 
            rounded-2xl
            text-lg text-body-color
            cursor-pointer
            font-medium
             bg-black text-white
            transition ease-in-out"
                target="_blank"
                rel="noreferrer"
                href="https://play.google.com/store/apps/details?id=club.findups.daily&hl=en_IN&gl=US"
              >
                Download App
              </a>
            </div>
          </div>
        </div>
      </div>
      {isMobile && (
        <button
          onClick={openModal}
          className="w-screen text-black font-medium text-xl fixed bottom-0 py-3 readmore-btn cursor-pointer"
        >
          Read More
        </button>
      )}

      {isMobile ? (
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h3 className="font-bold mb-5 ">Continue reading?</h3>
          <div className="flex flex-col w-full px-10 pt-3 items-center">
            <a
              target="_blank"
              rel="noreferrer"
              href="https://play.google.com/store/apps/details?id=club.findups.daily&hl=en_IN&gl=US"
              className="

inline-block
py-2
px-7
border 
rounded-full
text-base text-body-color
cursor-pointer
font-medium
hover:border-primary bg-black text-white
transition ease-in-out
"
            >
              Download App
            </a>
            <span className="my-5 or relative">Or</span>
            <a
              target="_blank"
              rel="noreferrer"
              href={newsData.news_url}
              className="
  
  inline-block
  py-2
  px-7
  border 
  rounded-full
  text-base text-body-color
  cursor-pointer
  font-medium
  hover:border-primary hover:bg-black hover:text-white
  transition ease-in-out
  "
            >
              Go to website
            </a>
          </div>
        </Modal>
      ) : (
        ""
      )}
    </section>
  );
}

export default NewsData;

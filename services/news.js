const axios = require("axios");
/**
 * Function to get Topics
 */
export const getTopics = async () => {
  const baseUrl = process.env.BASE_URL;
  try {
    const result = await axios.get(`${baseUrl}/website/topics`);
    const data = result.data.data;
    return data;
  } catch (error) {
    console.log(error);
  }
};


/**
 * Function to get news from pre defined topics
 * @param {string} topic 
 * @param {number} pageNo 
 * @returns array of news
 */
export const getNewsfromTopic = async (topic, pageNo) => {
  const baseUrl = process.env.BASE_URL;
  try {
    const result = await axios.get(
      `${baseUrl}/website/topicNews?limit=20&page=${pageNo}&keyword=${topic}`
    );
    const data = result.data.data;
    return data;
  } catch (error) {
    console.log(error);
  }
};

/**
 * 
 * @param {*} topic 
 * @param {*} page 
 * @returns 
 */
export const getNewsfromQuery = async (topic, page) => {
  const baseUrl = process.env.BASE_URL;
  try {
    const result = await axios.get(
      `${baseUrl}/website/searchNews?limit=20&page=${page}&q=${topic}`
    );
    const data = result.data.data;
    return data;
  } catch (error) {
    console.log(error);
  }
};

/**
 * 
 * @param {string} newsId 
 * @returns 
 */
 export const getNewsfromId = async (newsId) => {
  const baseUrl = process.env.BASE_URL;
  try {
    const result = await axios.get(
      `${baseUrl}/website/news?newsId=${newsId}`
    );
    const data = result.data.data;
    return data;
  } catch (error) {
    console.log(error);
  }
};
